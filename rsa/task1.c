#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <openssl/bn.h>

int str2hex(char **buf, const char *str, const int len) {
	*buf = (char *)malloc(len * 2 + 1);

	int i = 0;
	int k = 0;
	while (i < len) {
		sprintf((*buf) + k, "%02X", str[i]);
		i++;
		k += 2;
	}
	(*buf)[k] = '\0';
	return k;
}

void printBN(const char *before, BIGNUM *a) {
	printf("%s", before);
	BN_print_fp(stdout, a);
	puts("");
}

int main(void) {
	/*
	 	Task 1: Deriving the Private Key

		Let p, q, and e be three prime numbers. Let n = p*q. We will use (e, n) as the public key. Please
		calculate the private key d. The hexadecimal values of p, q, and e are listed in the following. It should be
		noted that although p and q used in this task are quite large numbers, they are not large enough to be secure.
		We intentionally make them small for the sake of simplicity. In practice, these numbers should be at least
		512 bits long (the one used here are only 128 bits).

		p = F7E75FDC469067FFDC4E847C51F452DF
		q = E85CED54AF57E53E092113E62F436F4F
		e = 0D88C3
	 */

	BN_CTX *ctx = BN_CTX_new();
	BIGNUM *p = BN_new();
	BIGNUM *q = BN_new();
	BIGNUM *e = BN_new();
	BIGNUM *one = BN_new();

	BN_hex2bn(&p, "F7E75FDC469067FFDC4E847C51F452DF");
	BN_hex2bn(&q, "E85CED54AF57E53E092113E62F436F4F");
	BN_hex2bn(&e, "0D88C3");
	BN_hex2bn(&one, "1");

	BIGNUM *n = BN_new();

	BN_mul(n, p, q, ctx);

	BN_sub(p, p, one);
	BN_sub(q, q, one);

	BIGNUM *phi = BN_new();

	BN_mul(phi, p, q, ctx);

	BIGNUM *d = BN_new();

	BN_mod_inverse(d, e, phi, ctx);

	printBN("d = ", d);
}
