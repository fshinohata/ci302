#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <openssl/bn.h>

int str2hex(char **buf, const char *str, const int len) {
	*buf = (char *)malloc(len * 2 + 1);

	int i = 0;
	int k = 0;
	while (i < len) {
		sprintf((*buf) + k, "%02X", str[i]);
		i++;
		k += 2;
	}
	(*buf)[k] = '\0';
	return k;
}

void printBN(const char *before, BIGNUM *a) {
	printf("%s", before);
	BN_print_fp(stdout, a);
	puts("");
}

int main(void) {
	/*
		Task 2: Encrypting a Message

		Let (e, n) be the public key. Please encrypt the message "A top secret!" (the quotations are not
		included). We need to convert this ASCII string to a hex string, and then convert the hex string to a BIGNUM
		using the hex-to-bn API BN hex2bn(). The following python command can be used to convert a plain
		ASCII string to a hex string.

		SEED Labs – RSA Public-Key Encryption and Signature Lab 5
		$ python -c ’print("A top secret!".encode("hex"))’
		4120746f702073656372657421

		The public keys are listed in the followings (hexadecimal). We also provide the private key d to help
		you verify your encryption result.

		n = DCBFFE3E51F62E09CE7032E2677A78946A849DC4CDDE3A4D0CB81629242FB1A5
		e = 010001 (this hex value equals to decimal 65537)
		M = A top secret!
		d = 74D806F9F3A62BAE331FFE3F0A68AFE35B3D2E4794148AACBC26AA381CD7D30D
	 */

	BN_CTX *ctx = BN_CTX_new();
	BIGNUM *n = BN_new();
	BIGNUM *e = BN_new();
	BIGNUM *d = BN_new();

	BN_hex2bn(&n, "DCBFFE3E51F62E09CE7032E2677A78946A849DC4CDDE3A4D0CB81629242FB1A5");
	BN_hex2bn(&e, "010001");
	BN_hex2bn(&d, "74D806F9F3A62BAE331FFE3F0A68AFE35B3D2E4794148AACBC26AA381CD7D30D");

	const char *message = "A top secret!";
	const int messageLen = strlen(message);

	char *messageHex;
	str2hex(&messageHex, message, messageLen);

	BIGNUM *m = BN_new();

	BN_hex2bn(&m, messageHex);

	BIGNUM *encrypted = BN_new();

	BN_mod_exp(encrypted, m, e, n, ctx);

	printBN("Encrypted message: ", encrypted);
}
