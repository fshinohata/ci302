#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <openssl/bn.h>

int str2hex(char **buf, const char *str, const int len) {
	*buf = (char *)malloc(len * 2 + 1);

	int i = 0;
	int k = 0;
	while (i < len) {
		sprintf((*buf) + k, "%02X", str[i]);
		i++;
		k += 2;
	}
	(*buf)[k] = '\0';
	return k;
}

int hex2str(char **buf, const char *hex, const int len) {
	*buf = (char *)malloc(len / 2 + 1);
	char aux[3] = { '\0', '\0', '\0' };

	int i = 0;
	int k = 0;
	while (i < len) {
		aux[0] = hex[i];
		aux[1] = hex[i + 1];
		(*buf)[k] = (char)strtol(aux, NULL, 16);
		i += 2;
		k++;
	}

	return k;
}

void printBN(const char *before, BIGNUM *a) {
	printf("%s", before);
	BN_print_fp(stdout, a);
	puts("");
}

int main(void) {
	/*
		 Task 3: Decrypting a Message

		The public/private keys used in this task are the same as the ones used in Task 2. Please decrypt the following
		ciphertext C, and convert it back to a plain ASCII string.

		C = 8C0F971DF2F3672B28811407E2DABBE1DA0FEBBBDFC7DCB67396567EA1E2493F

		You can use the following python command to convert a hex string back to to a plain ASCII string.

		$ python -c ’print("4120746f702073656372657421".decode("hex"))’
		A top secret!

		n = DCBFFE3E51F62E09CE7032E2677A78946A849DC4CDDE3A4D0CB81629242FB1A5
		e = 010001 (this hex value equals to decimal 65537)
		M = A top secret!
		d = 74D806F9F3A62BAE331FFE3F0A68AFE35B3D2E4794148AACBC26AA381CD7D30D
	 */

	BN_CTX *ctx = BN_CTX_new();
	BIGNUM *d = BN_new();
	BIGNUM *n = BN_new();
	BIGNUM *m = BN_new();

	BN_hex2bn(&m, "8C0F971DF2F3672B28811407E2DABBE1DA0FEBBBDFC7DCB67396567EA1E2493F");

	BN_hex2bn(&n, "DCBFFE3E51F62E09CE7032E2677A78946A849DC4CDDE3A4D0CB81629242FB1A5");
	BN_hex2bn(&d, "74D806F9F3A62BAE331FFE3F0A68AFE35B3D2E4794148AACBC26AA381CD7D30D");

	BIGNUM *decrypted = BN_new();

	BN_mod_exp(decrypted, m, d, n, ctx);

	printBN("Decrypted message: ", decrypted);

	char *hex = BN_bn2hex(decrypted);
	char *fromHex;
	hex2str(&fromHex, hex, strlen(hex));

	printf("Decoded message: %s\n", fromHex);
}
