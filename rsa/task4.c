#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <openssl/bn.h>

int str2hex(char **buf, const char *str, const int len) {
	*buf = (char *)malloc(len * 2 + 1);

	int i = 0;
	int k = 0;
	while (i < len) {
		sprintf((*buf) + k, "%02X", str[i]);
		i++;
		k += 2;
	}
	(*buf)[k] = '\0';
	return k;
}

int hex2str(char **buf, const char *hex, const int len) {
	*buf = (char *)malloc(len / 2 + 1);
	char aux[3] = { '\0', '\0', '\0' };

	int i = 0;
	int k = 0;
	while (i < len) {
		aux[0] = hex[i];
		aux[1] = hex[i + 1];
		(*buf)[k] = (char)strtol(aux, NULL, 16);
		i += 2;
		k++;
	}

	return k;
}

void printBN(const char *before, BIGNUM *a) {
	printf("%s", before);
	BN_print_fp(stdout, a);
	puts("");
}

int main(void) {
	/*
		Task 4: Signing a Message

		The public/private keys used in this task are the same as the ones used in Task 2. Please generate a signature
		for the following message (please directly sign this message, instead of signing its hash value):

		M = I owe you $2000.

		Please make a slight change to the message M, such as changing $2000 to $3000, and sign the modified
		message. Compare both signatures and describe what you observe.

		n = DCBFFE3E51F62E09CE7032E2677A78946A849DC4CDDE3A4D0CB81629242FB1A5
		e = 010001 (this hex value equals to decimal 65537)
		d = 74D806F9F3A62BAE331FFE3F0A68AFE35B3D2E4794148AACBC26AA381CD7D30D
	 */

	BN_CTX *ctx = BN_CTX_new();
	BIGNUM *n = BN_new();
	BIGNUM *d = BN_new();
	BIGNUM *e = BN_new();

	BN_hex2bn(&n, "DCBFFE3E51F62E09CE7032E2677A78946A849DC4CDDE3A4D0CB81629242FB1A5");
	BN_hex2bn(&d, "74D806F9F3A62BAE331FFE3F0A68AFE35B3D2E4794148AACBC26AA381CD7D30D");
	BN_hex2bn(&e, "010001");

	const char *messageOne = "I owe you $2000.";

	char *messageOneHex;
	str2hex(&messageOneHex, messageOne, strlen(messageOne));

	BIGNUM *m1 = BN_new();
	BN_hex2bn(&m1, messageOneHex);
	BN_mod_exp(m1, m1, e, n, ctx); // m1 = m1^e mod n

	char *m1Hex = BN_bn2hex(m1);
	printf("message: '%s'\nhex: '%s'\nsigned: '%s'\n", messageOne, messageOneHex, m1Hex);

	const char *messageTwo = "I owe you $3000.";

	char *messageTwoHex;
	str2hex(&messageTwoHex, messageTwo, strlen(messageTwo));

	BIGNUM *m2 = BN_new();
	BN_hex2bn(&m2, messageTwoHex);
	BN_mod_exp(m2, m2, e, n, ctx); // m2 = m2^e mod n

	char *m2Hex = BN_bn2hex(m2);
	printf("\nmessage: '%s'\nhex: '%s'\nsigned: '%s'\n", messageTwo, messageTwoHex, m2Hex);
}
