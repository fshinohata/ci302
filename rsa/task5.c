#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <openssl/bn.h>

int str2hex(char **buf, const char *str, const int len) {
	*buf = (char *)malloc(len * 2 + 1);

	int i = 0;
	int k = 0;
	while (i < len) {
		sprintf((*buf) + k, "%02X", str[i]);
		i++;
		k += 2;
	}
	(*buf)[k] = '\0';
	return k;
}

int hex2str(char **buf, const char *hex, const int len) {
	*buf = (char *)malloc(len / 2 + 1);
	char aux[3] = { '\0', '\0', '\0' };

	int i = 0;
	int k = 0;
	while (i < len) {
		aux[0] = hex[i];
		aux[1] = hex[i + 1];
		(*buf)[k] = (char)strtol(aux, NULL, 16);
		i += 2;
		k++;
	}

	return k;
}

void printBN(const char *before, BIGNUM *a) {
	printf("%s", before);
	BN_print_fp(stdout, a);
	puts("");
}

int main(void) {
	/*
		Task 5: Verifying a Signature

		Bob receives a message M = "Launch a missile." from Alice, with her signature S. We know that
		Alice’s public key is (e, n). Please verify whether the signature is indeed Alice’s or not. The public key
		and signature (hexadecimal) are listed in the following:

		M = Launch a missle.
		S = 643D6F34902D9C7EC90CB0B2BCA36C47FA37165C0005CAB026C0542CBDB6802F
		e = 010001 (this hex value equals to decimal 65537)
		n = AE1CD4DC432798D933779FBD46C6E1247F0CF1233595113AA51B450F18116115

		Suppose that the signature in is corrupted, such that the last byte of the signature changes from 2F to 3F,
		i.e, there is only one bit of change. Please repeat this task, and describe what will happen to the verification
		process
	 */

	BN_CTX *ctx = BN_CTX_new();
	BIGNUM *s = BN_new();
	BIGNUM *e = BN_new();
	BIGNUM *n = BN_new();

	const char *signedMessage = "643D6F34902D9C7EC90CB0B2BCA36C47FA37165C0005CAB026C0542CBDB6802F";

	BN_hex2bn(&s, signedMessage);
	BN_hex2bn(&e, "010001");
	BN_hex2bn(&n, "AE1CD4DC432798D933779FBD46C6E1247F0CF1233595113AA51B450F18116115");

	BIGNUM *decrypted = BN_new();

	BN_mod_exp(decrypted, s, e, n, ctx); // decrypted = s^e mod n

	char *decryptedHex = BN_bn2hex(decrypted);
	char *decryptedStr;
	hex2str(&decryptedStr, decryptedHex, strlen(decryptedHex));

	printf("\nsigned message (good): '%s'\ndecrypted: '%s' ('%s')\n", signedMessage, decryptedHex, decryptedStr);

	const char *corruptedSignedMessage = "643D6F34902D9C7EC90CB0B2BCA36C47FA37165C0005CAB026C0542CBDB6803F";
	BN_hex2bn(&s, corruptedSignedMessage);

	BN_mod_exp(decrypted, s, e, n, ctx); // decrypted = s^e mod n

	decryptedHex = BN_bn2hex(decrypted);
	hex2str(&decryptedStr, decryptedHex, strlen(decryptedHex));

	printf("\nsigned message (corrupted): '%s'\ndecrypted: '%s' ('%s')\n", signedMessage, decryptedHex, decryptedStr);
}
